package com.example.bleadvertisertest;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.nio.ByteBuffer;

public class MainActivity extends Activity {

    private BluetoothLeAdvertiser advertiser;
    private BluetoothAdapter btAdapter;
    private BluetoothManager btManager;
    private AdvertiseSettings advertiseSettings;
    private AdvertiseData advertiseData;
    private static final String UUID = "RandomCharacters";
    private AdvertiseCallback callback;
    private boolean advertisingStarted;
    private Thread thread;
    private boolean keepRunning = false;
    private long dummyData = 0;
    private TextView textViewStatus;
    private Runnable adRunnable;
    private long cycles = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewStatus = (TextView) findViewById(R.id.textViewStatus);

        adRunnable = new Runnable() {
            @Override
            public void run() {
                int counter = 0;
                while (keepRunning) {
                    Log.d(this.getClass().getName(), "START " + cycles++);
                    if (cycles > 2147483647) {
                        cycles = 0;
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textViewStatus.setText("Starting...");
                        }
                    });

                    advertisingStarted = false;
                    advertiser.startAdvertising(advertiseSettings, advertiseData, callback);

                    // Let advertising run for a while.
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // If the start has failed break the loop.
                    if (!advertisingStarted) {
                        Log.d(this.getClass().getName(), "ERROR");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textViewStatus.setText("Stopped because of an error.");
                            }
                        });
                        break;
                    }

                    // Stop advertising and modify the data.
                    Log.d(this.getClass().getName(), "STOP");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textViewStatus.setText("Stopping...");
                        }
                    });
                    advertiser.stopAdvertising(callback);
                    setAdvertiseData();
                }
            }
        };

        callback = new AdvertiseCallback() {
            /**
             * Callback when advertising could not be started.
             *
             * @param errorCode Error code (see ADVERTISE_FAILED_* constants) for advertising start
             *                  failures.
             */
            @Override
            public void onStartFailure(final int errorCode) {
                super.onStartFailure(errorCode);
                Log.d(this.getClass().getName(), "BLE advertising failed!");
                advertisingStarted = false;
                String errorText = "";

                switch (errorCode) {
                    case AdvertiseCallback.ADVERTISE_FAILED_ALREADY_STARTED:
                        errorText = "ADVERTISE_FAILED_ALREADY_STARTED";
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_DATA_TOO_LARGE:
                        errorText = "ADVERTISE_FAILED_DATA_TOO_LARGE";
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_FEATURE_UNSUPPORTED:
                        errorText = "ADVERTISE_FAILED_FEATURE_UNSUPPORTED";
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_INTERNAL_ERROR:
                        errorText = "ADVERTISE_FAILED_INTERNAL_ERROR";
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_TOO_MANY_ADVERTISERS:
                        errorText = "ADVERTISE_FAILED_TOO_MANY_ADVERTISERS";
                        break;
                    default:
                        errorText = "(Unknown error)"; // An impossible scenario
                }

                Log.d(this.getClass().getName(), errorText);
                textViewStatus.setText("Start failed: " + errorText);

                final AlertDialog alertDialogUpdateError =
                        new AlertDialog.Builder(MainActivity.this).create();
                alertDialogUpdateError.setTitle("BLE advertisement error");
                alertDialogUpdateError.setMessage("Error: " + errorText);
                alertDialogUpdateError.setButton(
                        "OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialogUpdateError.dismiss();
                            }
                        });
                alertDialogUpdateError.show();
            }

            /**
             * Callback triggered in response to {@link BluetoothLeAdvertiser#startAdvertising} indicating
             * that the advertising has been started successfully.
             *
             * @param settingsInEffect The actual settings used for advertising, which may be different from
             *                         what has been requested.
             */
            @Override
            public void onStartSuccess(AdvertiseSettings settingsInEffect) {
                super.onStartSuccess(settingsInEffect);
                Log.d(this.getClass().getName(), "BLE advertising started.");
                advertisingStarted = true;
            }
        };

        // Assuming that Bluetooth LE is supported and that Bluetooth is on. Should be checked
        // of course.
        btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();
        advertiser = btAdapter.getBluetoothLeAdvertiser();

        // Not checking BluetoothAdapter.isMultipleAdvertisementSupported() because it returns
        // false on Moto G. The device does support a single advertisement at a time though.
        if (advertiser == null) {
            Log.d(this.getClass().getName(), "Can not get advertiser!");
            final AlertDialog alertDialogNoAdvertiser =
                    new AlertDialog.Builder(MainActivity.this).create();
            alertDialogNoAdvertiser.setTitle("BLE advertisement error");
            alertDialogNoAdvertiser.setMessage("Can not get advertiser.");
            alertDialogNoAdvertiser.setButton(
                    "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialogNoAdvertiser.dismiss();
                        }
                    });
            alertDialogNoAdvertiser.show();
        }

        setAdvertiseData();
        setAdvertiseSettings();
    }

    @Override
    protected void onStart() {
        super.onStart();

        thread = new Thread(adRunnable);
        keepRunning = true;
        Log.d(this.getClass().getName(), "Starting BLE advertising...");
        thread.start();
    }

    @Override
    protected void onStop() {
        super.onStop();

        advertiser.stopAdvertising(callback);
        Log.d(this.getClass().getName(), "Stopping BLE advertising...");
        keepRunning = false;
    }


    public void setAdvertiseSettings() {
        AdvertiseSettings.Builder builder = new AdvertiseSettings.Builder();
        builder.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY);
        builder.setConnectable(false);
        builder.setTimeout(0);
        builder.setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH);
        advertiseSettings = builder.build();
    }

    public void setAdvertiseData() {
        AdvertiseData.Builder builder = new AdvertiseData.Builder();
        ByteBuffer manufacturerData = ByteBuffer.allocate(23);
        byte[] uuid = UUID.getBytes();

        manufacturerData.put(0, (byte) 0x02);
        manufacturerData.put(1, (byte) 0x15);
        for (int i=2; i<=17; i++) {
            manufacturerData.put(i, uuid[i - 2]); // UUID
        }

        manufacturerData.put(18, (byte) 0xFF); // first byte of Major
        manufacturerData.put(19, (byte) 0xFF); // second byte of Major
        manufacturerData.put(20, (byte) 0xFF); // first byte of minor
        manufacturerData.put(21, (byte) 0xFF); // second byte of minor
        // Increment and loop the second byte of minor.
        /*manufacturerData.put(21, (byte) dummyData); // second byte minor
        dummyData++;
        if (dummyData > 255) {
            dummyData = 0;
        }*/

        manufacturerData.put(22, (byte) 0xCE); // txPower (-50 dBm, seemed realistic)

        //builder.addManufacturerData(224, manufacturerData.array()); // Google
        builder.addManufacturerData(76, manufacturerData.array()); // Apple
        advertiseData = builder.build();
    }
}
